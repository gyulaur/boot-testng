package com.gyulaur;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.containsString;

@SpringBootTest
public class JunitApplicationTests extends AbstractTestNGSpringContextTests {

	private MockMvc mockMvc;

	@BeforeClass
	public void beforeClass() {
		mockMvc = MockMvcBuilders.webAppContextSetup((WebApplicationContext) applicationContext).build();
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testHealthCheck() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/health")).andExpect(
				MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string(containsString("UP")));
	}

}
